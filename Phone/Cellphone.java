package phone;
import java.lang.Math;

public class Cellphone implements Phone
{
    private String merk;
    private String type;
    private int batteryLevel;
    private int status;
    private int volume;

    // Menambahkan variabel pulsa untuk membantu Top Up Pulsa dan Sisa Pulsa
    private int pulsa;

    public Cellphone(String merk,String type)
    {
        this.merk = merk;
        this.type = type;
        this.batteryLevel = (int)(Math.random()*(100-0+1)+0);
        System.out.println("Merk HP : " + this.merk);
        System.out.println("Merk HP : " + this.type);
    }

    public void powerOn()
    {
        this.status = 1;
        System.out.println("Ponsel menyala");
    }

    public void powerOff()
    {
        this.status = 0;
        System.out.println("Ponsel mati");
    }

    //digunakan untuk menaikan volume
    public void volumeUp()
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel mati. Tidak dapat menaikkan volume");
        }
        else
        {
            this.volume++;
            if(this.volume>=100)
            {
                this.volume = 100;
            }
        }
    }

    //digunakan untuk menurunkan volume
    public void volumeDown() 
    {
        if(this.status == 0)
        {
            System.out.println("Ponsel mati. Tidak dapat menurunkan volume");
        }
        else if(this.status != 0)
        {
            this.volume--;
            if(this.volume<=0)
            {
                System.out.println("volume paling kecil");
            }
        }
    }

    public int getVolume()
    {
        return this.volume;
    }

    public int getBat()
    {
        if(this.status != 0)
        {
            if(this.batteryLevel>MIN_BATT_LEVEL && this.batteryLevel<=MAX_BATT_LEVEL)
            {
                System.out.println("Battery : " + this.batteryLevel);
                this.status = 1;
            } else if(this.batteryLevel == MIN_BATT_LEVEL)
            {
                System.out.println("Battery : " + this.batteryLevel);
                this.status=0;
            }
        }
        return this.batteryLevel;
    }

    // digunakan untuk melakukan pengisian pulsa ke ponsel.
    public int topUpPulsa(int Topup_pulsa)
    {
        if(this.status != 0)
        {
            if(Topup_pulsa < MINIMAL_PULSA)
            {
                System.out.println("Tidak dapat top up pulsa, minimal 10000");
            }else{
                this.pulsa = Topup_pulsa;
            }
        }else{
            System.out.println("HP mati mungkin pulsa sudah masuk, namun jika ingin melihat, hidupkan hp anda!");
        }
        return this.pulsa;
    }

    //digunakan untuk mengambil jumlah pulsa saat ini.
    public int getPulsa()
    {
        return this.pulsa;
    }

    // digunakan untuk menampilkan sisa pulsa ponsel
    public void sisaPulsa()
    {
        if(this.status != 0){
            System.out.println("Sisa Pulsa : " + this.pulsa);
        }
    }

    //digunakan untuk mengatur tingkat volume p
    public int setVol(int setVolume)
    {
        if(this.status != 0){
            this.volume = setVolume;
        }
        return this.volume;
    }
}
