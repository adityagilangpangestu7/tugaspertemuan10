package phone;

public class CellphoneMain {
    public static void main(String[] args)
    {
        Cellphone cp = new Cellphone("Nokia", "3310");
        cp.powerOn(); //untuk menghidupkan ponsel. Ini akan menampilkan pesan "Ponsel menyala".
        cp.powerOff(); //untuk mematikan ponsel. Ini akan menampilkan pesan "Ponsel mati".
        cp.volumeDown();
        cp.sisaPulsa();

        // Setelah dihidupkan function bisa digunakan kembali
        cp.powerOn();
        cp.volumeUp();
        cp.getVolume();
        cp.setVol(12);
        cp.getVolume();

        // Top Up pulsa diatas minimal 10000 & HP posisi hidup
        // ketika posisi mati pulsa terkirim namun tidak bisa terlihat
        cp.topUpPulsa(30000);
        cp.sisaPulsa();

        // Menambahkan banyak contact dengan Array List 
        Contact contact = new Contact("Gilang", "13492");
        contact.tambahContact("Nibras", "13515");
        contact.tambahContact("Azka", "13517");
        contact.tambahContact("Tito", "13516");
        contact.tambahContact("Eky", "13510");
        contact.displayContacts();

        // Mencari Contact dengan Nama dan No
        contact.showContactByName("Nibras");
        contact.showContactByNoHP("13515");

    }
}
