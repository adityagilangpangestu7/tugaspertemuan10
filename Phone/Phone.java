package phone;
public interface Phone
{
    public static final int MAX_VOLUME = 100;
    public static final int MIN_VOLUME = 0;
    public static final int MAX_BATT_LEVEL = 100;
    public static final int MIN_BATT_LEVEL = 0;

    //digunakan untuk minimal pulsa sebesar 10.000
    public static final int MINIMAL_PULSA = 10000;

    void powerOn();
    void powerOff();
    void volumeUp();
    void volumeDown();
    int getVolume();
    int getBat();

    //Method untuk Top Up Pulsa
    int topUpPulsa(int Topup_pulsa);
    int getPulsa(); 

    //Method untuk sisa pulsa
    void sisaPulsa();

    
}